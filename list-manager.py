import os
import sys
import random


def read_file(name: str) -> list:
    items = set()  # We start with a set to handle possible deduplication of items
    with open(name) as file:
        for line in file.readlines():
            line = line.replace("\n", "")
            if line:  # i.e. if not empty
                items.add(line)
    return sorted(list(items))


def write_file(name: str, data: iter) -> None:
    with open(name, mode="w") as file:
        file.writelines(sorted([line + "\n" for line in data]))


def prompt_with_menu(prompt: str, first_choice: int, last_choice: int) -> int:
    print(prompt, end="")
    choice = int(input(""))
    while not (first_choice <= choice <= last_choice):
        print(prompt, end="")
        choice = int(input(""))
    return choice


def add_item(file_path: str) -> None:
    items = read_file(file_path)
    print("Leave empty to go back")  # TODO Make slanted using ANSI codes
    while True:
        item_candidate = input("Item to add: ")
        if not item_candidate:  # If empty, basically
            return
        elif item_candidate not in items:
            items.append(item_candidate)
            write_file(file_path, items)


def get_random_item(file_path: str) -> None:
    items = read_file(file_path)
    while True:
        item_index = random.randrange(0, len(items))
        item: str = items[item_index]  # Type stuff so IDE doesn't complain
        choice = prompt_with_menu(item
                                  + "\n0. Back"
                                  + " " * 4 + "1. Remove"
                                  + " " * 4 + "2. Next"
                                  + " " * 4 + "3. Edit\n", 0, 3)
        if choice == 0:
            return
        elif choice == 1:
            del (items[item_index])
            write_file(file_path, items)
        elif choice == 2:
            continue
        else:
            items[item_index] = input("New version of item: ")
            write_file(file_path, items)


def main():
    # I just don't like variables in "main" having a global scope (the IDE complaining doesn't help, either).
    # This idea of having global statements as "main" feels too weird for me, coming from C++.
    if len(sys.argv) != 2:
        print("\033[31mNeed to supply exactly one argument (where to read from or make the wishlist)")
        exit(1)
    file_path = sys.argv[1]
    random.seed()

    # User menu phase
    while True:
        choice = prompt_with_menu(f"{file_path}:\n"
                                  + "\t0. Exit\n"
                                  + "\t1. Add item\n"
                                  + "\t2. Get random item\n"
                                  + "\t3. Edit with vim\n", 0, 3)  # TODO Add searching
        if choice == 0:
            exit(0)
        elif choice == 1:
            add_item(file_path)
        elif choice == 2:
            get_random_item(file_path)
        else:
            os.system(f'vim "{file_path}"')


main()
